package br.edu.unisep.instantpay.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.security.Key;

@Data
@Entity
@Table(name = "customer_key")
public class CustomerKey {

    @Id
    @Column(name = "id_customerkey")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "id_keytype")
    private KeyType keyType;

    @OneToOne
    @JoinColumn(name = "id_bank")
    private Bank bank;

    @OneToOne
    @JoinColumn(name = "id_customer")
    private Customer customer;

    @Column(name = "key")
    private String key;

    @Column(name = "account")
    private Integer account;

    @Column(name = "agency")
    private Integer agency;

}

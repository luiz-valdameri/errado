package br.edu.unisep.instantpay.data.repository;

import br.edu.unisep.instantpay.data.entity.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankRepository extends JpaRepository<Bank, Integer> {
}

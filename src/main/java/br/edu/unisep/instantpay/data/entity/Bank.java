package br.edu.unisep.instantpay.data.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "bank")
public class Bank {

    @Id
    @Column(name = "id_bank")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

}

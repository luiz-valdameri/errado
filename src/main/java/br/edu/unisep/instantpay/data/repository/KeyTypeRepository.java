package br.edu.unisep.instantpay.data.repository;

import br.edu.unisep.instantpay.data.entity.KeyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KeyTypeRepository extends JpaRepository<KeyType, Integer> {
}

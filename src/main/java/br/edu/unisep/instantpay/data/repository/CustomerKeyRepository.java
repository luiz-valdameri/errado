package br.edu.unisep.instantpay.data.repository;

import br.edu.unisep.instantpay.data.entity.CustomerKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerKeyRepository extends JpaRepository<CustomerKey, Integer> {

}

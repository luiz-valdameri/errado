package br.edu.unisep.instantpay.data.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "key_type")
public class KeyType {

    @Id
    @Column(name = "id_keytype")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

}

package br.edu.unisep.instantpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InstantPayApplication {

	public static void main(String[] args) {
		SpringApplication.run(InstantPayApplication.class, args);
	}

}
